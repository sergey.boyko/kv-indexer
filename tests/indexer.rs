#[macro_use]
extern crate serde_derive;

use async_trait::async_trait;
use futures::executor::block_on;
use itertools::Itertools;
use kv_indexer::{
    Bytes, IndexIterator, IndexValue, Indexer, ItemId, ItemIndex, KeyValueStore, Result,
    Serializer, TableItem,
};
use serde::de::DeserializeOwned;
use serde::Serialize;
use serde_json::{self as json, json, Value as Json};
use std::cmp::Ordering;
use std::collections::{BTreeMap, BTreeSet};

#[derive(Default)]
struct JsonSerializer;

impl Serializer for JsonSerializer {
    fn serialize<T: Serialize>(&mut self, value: T) -> Result<Bytes> {
        Ok(json::to_vec(&value)?)
    }

    fn deserialize<T: DeserializeOwned>(&mut self, bytes: Bytes) -> Result<T> {
        Ok(json::from_slice(&bytes)?)
    }
}

#[derive(Clone, Debug, Deserialize, Eq, Ord, PartialEq, PartialOrd, Serialize)]
struct UserInfo {
    login: String,
    budget: u64,
    age: u64,
}

impl UserInfo {
    fn valid_users_for_tests() -> Vec<UserInfo> {
        let user1 = UserInfo {
            login: "example_login".to_owned(),
            budget: 100500,
            age: 31,
        };
        let user2 = UserInfo {
            login: "another_login".to_owned(),
            budget: 42566,
            age: 88,
        };
        let user3 = UserInfo {
            login: "foo_login".to_owned(),
            budget: 63,
            age: 88,
        };
        let user4 = UserInfo {
            login: "bar_login".to_owned(),
            budget: 15151,
            age: 44,
        };
        vec![user1, user2, user3, user4]
    }
}

/// As I said above, it's better to implement a derive macro.
impl TableItem for UserInfo {
    fn is_unique(index_name: &'static str) -> Option<bool> {
        match index_name {
            "login" => Some(true),
            "age" => Some(false),
            _ => None,
        }
    }

    fn indexes(&self) -> Vec<ItemIndex> {
        vec![
            ItemIndex {
                index_name: "login",
                index_value: self.login.clone().into(),
                unique: true,
            },
            ItemIndex {
                index_name: "age",
                index_value: self.age.into(),
                unique: false,
            },
        ]
    }
}

struct TestStore(BTreeMap<Bytes, Bytes>);

#[async_trait]
impl KeyValueStore for TestStore {
    async fn get(&mut self, key: Bytes) -> Result<Option<Bytes>> {
        Ok(self.0.get(&key).cloned())
    }

    async fn set(&mut self, key: Bytes, value: Bytes) -> Result<()> {
        self.0.insert(key, value);
        Ok(())
    }
}

fn debug_store(store: &BTreeMap<Bytes, Bytes>) {
    for (key, value) in store {
        let key: Json =
            json::from_slice(&key).expect("JsonSerializer is expected to be used in tests");
        let value: Json =
            json::from_slice(&value).expect("JsonSerializer is expected to be used in tests");
        println!("{} : {}", key, value);
    }
}

#[test]
fn test_insert_item() {
    fn insert_json<K: Serialize, V: Serialize>(
        store: &mut BTreeMap<Bytes, Bytes>,
        key: K,
        value: V,
    ) {
        let key = json::to_vec(&key).unwrap();
        let value = json::to_vec(&value).unwrap();
        store.insert(key, value);
    }

    let store = TestStore(BTreeMap::new());
    let indexer: Indexer<UserInfo, TestStore, JsonSerializer> = Indexer::from_store(store);
    let mut expected_kv_store = BTreeMap::new();

    let user1 = UserInfo {
        login: "login1".to_owned(),
        budget: 100500,
        age: 18,
    };
    let serialized_user1 = json::to_value(&user1).unwrap();

    // A user with the same age as the user above.
    let user2 = UserInfo {
        login: "login2".to_owned(),
        budget: 30999,
        age: 18,
    };
    let serialized_user2 = json::to_value(&user2).unwrap();

    // Insert 'user1' and 'user2' using `indexer`.

    let user1_id = block_on(indexer.insert(user1.clone())).expect("Error inserting valid user1");
    let user2_id = block_on(indexer.insert(user2.clone())).expect("Error inserting valid user2");

    // Generate, serialize and insert `user1` and `user2` items into `expected_kv_store`.

    let user1_key = json!({
        "key_type": "item",
        "key": user1_id,
    });
    insert_json(&mut expected_kv_store, user1_key, serialized_user1);

    let user2_key = json!({
        "key_type": "item",
        "key": user2_id,
    });
    insert_json(&mut expected_kv_store, user2_key, serialized_user2);

    // Generate, serialize and insert indexes of `user1` and `user2` into `expected_kv_store`.

    let login_index_key = json!({
        "key_type": "unique_index",
        "key": "login",
    });
    // Please note this array is sorted by the `login` column.
    assert!(user1.login < user2.login);
    let login_index_value = json!([
        [user1.login.clone(), user1_id],
        [user2.login.clone(), user2_id]
    ]);
    insert_json(&mut expected_kv_store, login_index_key, login_index_value);

    let age_index_key = json!({
        "key_type": "index",
        "key": "age",
    });
    let age_index_value = json!([[user1.age, [user1_id, user2_id]]]);
    insert_json(&mut expected_kv_store, age_index_key, age_index_value);

    // Generate, serialize and insert login certain indexes of `user1` and `user2` into `expected_kv_store`.

    let user1_login_certain_key = json!({
        "key_type": "certain_unique_index",
        "key": {
            "index_name": "login",
            "index_value": user1.login.clone(),
        }
    });
    insert_json(&mut expected_kv_store, user1_login_certain_key, user1_id);

    let user2_login_key = json!({
        "key_type": "certain_unique_index",
        "key": {
            "index_name": "login",
            "index_value": user2.login.clone(),
        }
    });
    insert_json(&mut expected_kv_store, user2_login_key, user2_id);

    // Generate, serialize and insert age certain indexes of `user1` and `user2` into `expected_kv_store`.

    let age_key = json!({
        "key_type": "certain_index",
        "key": {
            "index_name": "age",
            "index_value": user1.age, // user1.age == user2.age
        }
    });
    insert_json(&mut expected_kv_store, age_key, vec![user1_id, user2_id]);

    // Compare `actual_kv_store` and `expected_kv_store`.

    let mut actual_kv_store = match indexer.__store() {
        Ok(store) => store.0,
        Err(_indexer) => panic!("There are unexpected pointers to the indexer already"),
    };

    // Generate, serialize and insert ids of `user1` and `user2`.
    // Please note we can't insert the ids into `epxected_kv_store`,
    // because `HashSet` does not guarantee an order of items.
    // So let's remove the set of item ids from `actual_kv_store` and compare it separately.

    let item_id_key = json!({ "key_type": "item_id" });
    let serialized_item_id_key = json::to_vec(&item_id_key).unwrap();
    let serialized_actual_item_id_value = actual_kv_store
        .remove(&serialized_item_id_key)
        .expect("'item_id' key not found");
    let actual_item_id_value: BTreeSet<ItemId> = json::from_slice(&serialized_actual_item_id_value)
        .expect("Error deserializing set of ItemId");
    let expected_item_id_value: BTreeSet<_> = vec![user1_id, user2_id].into_iter().collect();
    assert_eq!(actual_item_id_value, expected_item_id_value);

    // Continue comparing.

    println!("Actual");
    debug_store(&actual_kv_store);
    println!("Expected");
    debug_store(&expected_kv_store);

    assert_eq!(actual_kv_store, expected_kv_store);
}

#[test]
fn test_insert_unique_index_twice() {
    let store = TestStore(BTreeMap::new());
    let indexer: Indexer<UserInfo, TestStore, JsonSerializer> = Indexer::from_store(store);

    block_on(indexer.insert(UserInfo {
        login: "example_login".to_owned(),
        budget: 100500,
        age: 31,
    }))
    .expect("Error inserting valid user info");

    block_on(indexer.insert(UserInfo {
        login: "example_login".to_owned(),
        budget: 42566,
        age: 88,
    }))
    .expect_err("Expected an error on attempt to insert an item with the same unique index");
}

#[test]
fn test_filter_iterator() {
    let store = TestStore(BTreeMap::new());
    let indexer: Indexer<UserInfo, TestStore, JsonSerializer> = Indexer::from_store(store);

    let users = UserInfo::valid_users_for_tests();
    for user in users.iter() {
        block_on(indexer.insert(user.clone())).expect("Error inserting valid user info");
    }

    let fut = async move {
        indexer
            .iter()
            .await
            .filter("age", IndexValue::Number(43), Ordering::Greater)
            .unwrap()
            .collect_items()
            .await
            .expect("Error collecting filtered items")
            .into_iter()
            .sorted()
            .collect()
    };
    let actual_items: Vec<UserInfo> = block_on(fut);
    let expected_items: Vec<UserInfo> = vec![users[1].clone(), users[2].clone(), users[3].clone()]
        .into_iter()
        .sorted()
        .collect();
    assert_eq!(actual_items, expected_items);
}

#[test]
fn test_find_iterator() {
    let store = TestStore(BTreeMap::new());
    let indexer: Indexer<UserInfo, TestStore, JsonSerializer> = Indexer::from_store(store);

    let users = UserInfo::valid_users_for_tests();
    for user in users.iter() {
        block_on(indexer.insert(user.clone())).expect("Error inserting valid user info");
    }

    let fut = async move {
        indexer
            .find("age", IndexValue::Number(88))
            .await
            .unwrap()
            .collect_items()
            .await
            .expect("Error finding an item")
            .into_iter()
            .sorted()
            .collect()
    };
    let actual_items: Vec<UserInfo> = block_on(fut);
    let expected_items: Vec<UserInfo> = vec![users[1].clone(), users[2].clone()]
        .into_iter()
        .sorted()
        .collect();
    assert_eq!(actual_items, expected_items);
}

#[test]
fn test_find_unique_iterator() {
    let store = TestStore(BTreeMap::new());
    let indexer: Indexer<UserInfo, TestStore, JsonSerializer> = Indexer::from_store(store);

    let users = UserInfo::valid_users_for_tests();
    for user in users.iter() {
        block_on(indexer.insert(user.clone())).expect("Error inserting valid user info");
    }

    let fut = async move {
        indexer
            .find("login", IndexValue::String("example_login".to_owned()))
            .await
            .unwrap()
            .collect_items()
            .await
            .expect("Error finding an item")
    };
    let actual_items: Vec<UserInfo> = block_on(fut);
    let expected_items: Vec<UserInfo> = vec![users[0].clone()];
    assert_eq!(actual_items, expected_items);
}
