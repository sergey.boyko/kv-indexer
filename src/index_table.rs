use crate::btree_seq::BTreeSequence;
use crate::utils::{KeyValueItem, StoreKey, WithDefaultValueIfNotFound};
use crate::{IndexValue, ItemId};

/// The key of `IndexTableItem` or `UniqueIndexTableItem`.
/// Let `TableItem` has the `login` index.
/// Then this structure represents a name of the index i.e `login`.
#[derive(Clone, Debug, Deserialize, Serialize)]
pub(crate) struct IndexName(String);

impl IndexName {
    pub(crate) fn new(index_name: &'static str) -> IndexName {
        IndexName(index_name.to_owned())
    }
}

/// The item of a table that consists of the `IndexName` key and a set of pairs `(IndexValue, ItemId)`.
/// Let `TableItem` has the `email` index.
/// Then this structure will represent a row:
/// | "email" | [ "example@gmail.com", item1_id ], [ "another@gmail.com", item5_id ], ... |
pub(crate) struct UniqueIndexTableItem {
    key: IndexName,
    pub(crate) value: BTreeSequence<IndexValue, ItemId>,
}

impl KeyValueItem for UniqueIndexTableItem {
    type Key = IndexName;
    type Value = BTreeSequence<IndexValue, ItemId>;

    fn key_into_store_key(key: Self::Key) -> StoreKey {
        StoreKey::UniqueIndex(key)
    }

    fn into_key_value(self) -> (Self::Key, Self::Value) {
        (self.key, self.value)
    }

    fn from_key_value(key: Self::Key, value: Self::Value) -> Self {
        UniqueIndexTableItem { key, value }
    }
}

impl WithDefaultValueIfNotFound for UniqueIndexTableItem {
    type Value = BTreeSequence<IndexValue, ItemId>;
}

impl UniqueIndexTableItem {
    /// Returns a previous item_id if it was in the table by the given `index_value` already.
    pub(crate) fn insert_index(
        &mut self,
        index_value: IndexValue,
        item_id: ItemId,
    ) -> Option<ItemId> {
        self.value.insert(index_value, item_id)
    }
}

/// The item of a table that consists of the `IndexName` key and a sequence of pairs `(IndexValue, ItemId)`.
/// Let `TableItem` has the `email` index.
/// Then this structure will represent a row:
/// | "email" | [ [ "example@gmail.com", [item1_id, item2_id] ], ... ], [ [ "another@gmail.com", [item5_id, item10_id, ...] ] ], ... |
pub(crate) struct IndexTableItem {
    key: IndexName,
    pub(crate) value: BTreeSequence<IndexValue, Vec<ItemId>>,
}

impl KeyValueItem for IndexTableItem {
    type Key = IndexName;
    type Value = BTreeSequence<IndexValue, Vec<ItemId>>;

    fn key_into_store_key(key: Self::Key) -> StoreKey {
        StoreKey::Index(key)
    }

    fn into_key_value(self) -> (Self::Key, Self::Value) {
        (self.key, self.value)
    }

    fn from_key_value(key: Self::Key, value: Self::Value) -> Self {
        IndexTableItem { key, value }
    }
}

impl WithDefaultValueIfNotFound for IndexTableItem {
    type Value = BTreeSequence<IndexValue, Vec<ItemId>>;
}

impl IndexTableItem {
    pub(crate) fn insert_index(&mut self, index_value: IndexValue, item_id: ItemId) {
        self.value.entry(index_value).or_default().push(item_id)
    }
}
