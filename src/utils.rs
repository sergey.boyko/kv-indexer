use crate::certain_index_table::CertainIndexTableKey;
use crate::index_table::IndexName;
use crate::{ItemId, KeyValueStore, Serializer};
use anyhow::Result;
use serde::de::DeserializeOwned;
use serde::Serialize;

/// This enum represents a type of the key of the key-value store.
#[derive(Debug, Deserialize, Serialize)]
#[serde(tag = "key_type", content = "key", rename_all = "snake_case")]
pub(crate) enum StoreKey {
    Index(IndexName),
    UniqueIndex(IndexName),
    CertainIndex(CertainIndexTableKey),
    CertainUniqueIndex(CertainIndexTableKey),
    Item(ItemId),
    ItemId,
}

pub(crate) trait KeyValueItem {
    type Key;
    type Value;

    /// Different structures that implement `FromIntoKeyValue` may have the same [`FromIntoKeyValue::Key`] type,
    /// but every such structure is expected to convert [`FromIntoKeyValue::Key`] into `StoreKey` differently.
    fn key_into_store_key(key: Self::Key) -> StoreKey;

    fn into_key_value(self) -> (Self::Key, Self::Value);

    fn from_key_value(key: Self::Key, value: Self::Value) -> Self;
}

pub(crate) trait WithDefaultValueIfNotFound {
    type Value: Default;
}

pub(crate) async fn load_table_item_or_default<Item, K, V, Store, S>(
    store: &mut Store,
    serializer: &mut S,
    key: K,
) -> Result<Item>
where
    Item: KeyValueItem<Key = K, Value = V> + WithDefaultValueIfNotFound<Value = V>,
    K: Clone,
    V: DeserializeOwned + Default,
    Store: KeyValueStore,
    S: Serializer,
{
    let serialized_key = serializer.serialize(Item::key_into_store_key(key.clone()))?;
    let value = match store.get(serialized_key).await? {
        Some(value) => serializer.deserialize(value)?,
        None => V::default(),
    };
    Ok(Item::from_key_value(key, value))
}

pub(crate) async fn load_table_item<Item, K, V, Store, S>(
    store: &mut Store,
    serializer: &mut S,
    key: K,
) -> Result<Option<Item>>
where
    Item: KeyValueItem<Key = K, Value = V>,
    K: Clone,
    V: DeserializeOwned,
    Store: KeyValueStore,
    S: Serializer,
{
    let serialized_key = serializer.serialize(Item::key_into_store_key(key.clone()))?;
    let value = match store.get(serialized_key).await? {
        Some(value) => serializer.deserialize(value)?,
        None => return Ok(None),
    };
    Ok(Some(Item::from_key_value(key, value)))
}

pub(crate) async fn upload_table_item<Item, K, V, Store, S>(
    store: &mut Store,
    serializer: &mut S,
    item: Item,
) -> Result<()>
where
    Item: KeyValueItem<Key = K, Value = V>,
    K: Clone,
    V: Serialize,
    Store: KeyValueStore,
    S: Serializer,
{
    let (key, value) = item.into_key_value();
    let key = serializer.serialize(Item::key_into_store_key(key))?;
    let value = serializer.serialize(value)?;
    store.set(key, value).await
}
