use crate::utils::{KeyValueItem, StoreKey, WithDefaultValueIfNotFound};
use crate::ItemId;
use std::collections::HashSet;

#[derive(Clone, Copy)]
pub struct ItemIdTableKey;

/// The item of a table that consists of the `ItemId` key and the `T` value.
pub(crate) struct ItemIdTable {
    key: ItemIdTableKey,
    pub(crate) value: HashSet<ItemId>,
}

impl KeyValueItem for ItemIdTable {
    type Key = ItemIdTableKey;
    type Value = HashSet<ItemId>;

    fn key_into_store_key(_key: Self::Key) -> StoreKey {
        StoreKey::ItemId
    }

    fn into_key_value(self) -> (Self::Key, Self::Value) {
        (self.key, self.value)
    }

    fn from_key_value(key: Self::Key, value: Self::Value) -> Self {
        ItemIdTable { key, value }
    }
}

impl WithDefaultValueIfNotFound for ItemIdTable {
    type Value = HashSet<ItemId>;
}

impl ItemIdTable {
    pub(crate) fn insert(&mut self, item_id: ItemId) -> bool {
        self.value.insert(item_id)
    }
}
