use crate::index_table::{IndexName, IndexTableItem, UniqueIndexTableItem};
use crate::indexer::IndexerLock;
use crate::iterator::IndexIterator;
use crate::utils::load_table_item_or_default;
use crate::{ItemId, ItemIndex, KeyValueStore, Serializer, TableItem};
use anyhow::Result;
use async_trait::async_trait;
use serde::de::DeserializeOwned;
use std::iter::{once, FromIterator};

pub struct Find<'a, Item, Store, Ser> {
    /// Wrap `IndexerLock` into `Option` to move it to the next iterators.
    pub(crate) indexer: Option<IndexerLock<'a, Item, Store, Ser>>,
    pub(crate) index: ItemIndex,
}

#[async_trait]
impl<'a, Item, Store, Ser> IndexIterator<'a, Item, Store, Ser> for Find<'a, Item, Store, Ser>
where
    Item: TableItem + DeserializeOwned + Send,
    Store: KeyValueStore + Send,
    Ser: Serializer + Send,
{
    async fn collect_item_ids(
        &mut self,
        store: &mut Store,
        serializer: &mut Ser,
    ) -> Result<Vec<ItemId>> {
        self.collect_impl(store, serializer).await
    }

    fn __move_indexer(&mut self) -> Option<IndexerLock<'a, Item, Store, Ser>> {
        self.indexer.take()
    }
}

impl<'a, Item, Store, Ser> Find<'a, Item, Store, Ser>
where
    Item: DeserializeOwned + Send,
    Store: KeyValueStore + Send,
    Ser: Serializer + Send,
{
    async fn collect_impl(
        &mut self,
        store: &mut Store,
        serializer: &mut Ser,
    ) -> Result<Vec<ItemId>> {
        let key = IndexName::new(self.index.index_name);
        let item_ids = if self.index.unique {
            let mut index_table_item: UniqueIndexTableItem =
                load_table_item_or_default(store, serializer, key).await?;
            match index_table_item.value.remove_entry(&self.index.index_value) {
                Some((_index_value, item_id)) => Vec::from_iter(once(item_id)),
                None => Vec::new(),
            }
        } else {
            let mut index_table_item: IndexTableItem =
                load_table_item_or_default(store, serializer, key).await?;
            match index_table_item.value.remove(&self.index.index_value) {
                Some(items) => items,
                None => Vec::new(),
            }
        };
        Ok(item_ids)
    }

    fn __move_indexer(&mut self) -> Option<IndexerLock<'a, Item, Store, Ser>> {
        self.indexer.take()
    }
}
