use crate::item_table::ItemTable;
use crate::utils::load_table_item;
use crate::{IndexValue, ItemId, ItemIndex, KeyValueStore, Serializer, TableItem};
use anyhow::{anyhow, bail, Result};
use async_trait::async_trait;
use log::warn;
use serde::de::DeserializeOwned;
use std::cmp::Ordering;

mod filter;
mod find;
mod iter;

use crate::indexer::IndexerLock;
pub use filter::Filter;
pub use find::Find;
pub use iter::Iter;

#[async_trait]
pub trait IndexIterator<'a, Item, Store, Ser>
where
    Self: Sized,
    Item: TableItem + DeserializeOwned + Send + 'a,
    Store: KeyValueStore + Send + 'a,
    Ser: Serializer + Send + 'a,
{
    fn filter(
        mut self,
        index_name: &'static str,
        index_value: IndexValue,
        filter_ord: Ordering,
    ) -> Result<Filter<'a, Item, Store, Ser, Self>> {
        let unique = Item::is_unique(index_name).ok_or_else(|| anyhow!("Unknown index name"))?;
        let index = ItemIndex {
            index_name,
            index_value,
            unique,
        };

        let indexer = self.__move_indexer();
        Ok(Filter {
            indexer,
            prev_iter: self,
            index,
            filter_ord,
        })
    }

    async fn collect_items(mut self) -> Result<Vec<Item>> {
        let mut indexer = match self.__move_indexer() {
            Some(indexer) => indexer,
            None => bail!("Internal error: invalid 'indexer'"),
        };
        let store = indexer.store();
        let mut serializer = Ser::default();
        let item_ids = self.collect_item_ids(store, &mut serializer).await?;
        load_items(store, &mut serializer, item_ids).await
    }

    async fn collect_item_ids(
        &mut self,
        store: &mut Store,
        serializer: &mut Ser,
    ) -> Result<Vec<ItemId>>;

    /// Shouldn't be used outside of the crate.
    fn __move_indexer(&mut self) -> Option<IndexerLock<'a, Item, Store, Ser>>;
}

async fn load_items<Item, Store, Ser>(
    store: &mut Store,
    serializer: &mut Ser,
    item_ids: Vec<ItemId>,
) -> Result<Vec<Item>>
where
    Item: DeserializeOwned,
    Store: KeyValueStore,
    Ser: Serializer + Send,
{
    let mut items = Vec::new();
    for item_id in item_ids {
        let item_opt: Option<ItemTable<Item>> = load_table_item(store, serializer, item_id).await?;
        let item = match item_opt {
            Some(item) => item.value,
            None => {
                warn!("`ItemTable` contains unknown ItemId '{}'", item_id);
                continue;
            }
        };
        items.push(item);
    }
    Ok(items)
}
