use crate::index_table::{IndexName, IndexTableItem, UniqueIndexTableItem};
use crate::indexer::IndexerLock;
use crate::iterator::IndexIterator;
use crate::utils::load_table_item_or_default;
use crate::{ItemId, ItemIndex, KeyValueStore, Serializer, TableItem};
use anyhow::Result;
use async_trait::async_trait;
use serde::de::DeserializeOwned;
use std::cmp::Ordering;
use std::collections::HashSet;

pub struct Filter<'a, Item, Store, Ser, PrevIter> {
    pub(crate) indexer: Option<IndexerLock<'a, Item, Store, Ser>>,
    pub(crate) prev_iter: PrevIter,
    pub(crate) index: ItemIndex,
    pub(crate) filter_ord: Ordering,
}

#[async_trait]
impl<'a, Item, Store, Ser, PrevIter> IndexIterator<'a, Item, Store, Ser>
    for Filter<'a, Item, Store, Ser, PrevIter>
where
    Item: TableItem + DeserializeOwned + Send,
    Store: KeyValueStore + Send,
    Ser: Serializer + Send,
    PrevIter: IndexIterator<'a, Item, Store, Ser> + Send,
{
    async fn collect_item_ids(
        &mut self,
        store: &mut Store,
        serializer: &mut Ser,
    ) -> Result<Vec<ItemId>> {
        let prev_item_ids = self.prev_iter.collect_item_ids(store, serializer).await?;
        let filtered_item_ids = self.collect_filtered_item_ids(store, serializer).await?;
        let item_ids = prev_item_ids
            .into_iter()
            .filter(|item_id| filtered_item_ids.contains(item_id))
            .collect();
        Ok(item_ids)
    }

    fn __move_indexer(&mut self) -> Option<IndexerLock<'a, Item, Store, Ser>> {
        self.indexer.take()
    }
}

impl<'a, Item, Store, Ser, PrevIter> Filter<'a, Item, Store, Ser, PrevIter>
where
    Store: KeyValueStore,
    Ser: Serializer + Send,
{
    async fn collect_filtered_item_ids(
        &mut self,
        store: &mut Store,
        serializer: &mut Ser,
    ) -> Result<HashSet<ItemId>> {
        let key = IndexName::new(self.index.index_name);
        let item_ids = if self.index.unique {
            let index_table_item: UniqueIndexTableItem =
                load_table_item_or_default(store, serializer, key).await?;
            index_table_item
                .value
                .iter()
                .filter(|(index_value, _)| {
                    index_value.cmp(&&self.index.index_value) == self.filter_ord
                })
                .map(|(_index_value, item_id)| *item_id)
                .collect()
        } else {
            let index_table_item: IndexTableItem =
                load_table_item_or_default(store, serializer, key).await?;
            index_table_item
                .value
                .iter()
                .filter(|(index_value, _)| {
                    index_value.cmp(&&self.index.index_value) == self.filter_ord
                })
                .map(|(_index_value, item_ids)| item_ids)
                .flatten()
                .copied()
                .collect()
        };
        Ok(item_ids)
    }
}
