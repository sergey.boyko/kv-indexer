use crate::indexer::IndexerLock;
use crate::item_id_table::{ItemIdTable, ItemIdTableKey};
use crate::iterator::IndexIterator;
use crate::utils::load_table_item_or_default;
use crate::{ItemId, KeyValueStore, Serializer, TableItem};
use anyhow::Result;
use async_trait::async_trait;
use serde::de::DeserializeOwned;

pub struct Iter<'a, Item, Store, Ser> {
    /// Wrap `IndexerLock` into `Option` to move it to the next iterators.
    pub(crate) indexer: Option<IndexerLock<'a, Item, Store, Ser>>,
}

#[async_trait]
impl<'a, Item, Store, Ser> IndexIterator<'a, Item, Store, Ser> for Iter<'a, Item, Store, Ser>
where
    Item: TableItem + DeserializeOwned + Send,
    Store: KeyValueStore + Send,
    Ser: Serializer + Send,
{
    async fn collect_item_ids(
        &mut self,
        store: &mut Store,
        serializer: &mut Ser,
    ) -> Result<Vec<ItemId>> {
        self.collect_impl(store, serializer).await
    }

    fn __move_indexer(&mut self) -> Option<IndexerLock<'a, Item, Store, Ser>> {
        self.indexer.take()
    }
}

impl<'a, Item, Store, Ser> Iter<'a, Item, Store, Ser>
where
    Item: DeserializeOwned + Send,
    Store: KeyValueStore + Send,
    Ser: Serializer + Send,
{
    async fn collect_impl(
        &mut self,
        store: &mut Store,
        serializer: &mut Ser,
    ) -> Result<Vec<ItemId>> {
        let index_table_item: ItemIdTable =
            load_table_item_or_default(store, serializer, ItemIdTableKey).await?;
        Ok(index_table_item.value.into_iter().collect())
    }
}
