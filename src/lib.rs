#[macro_use]
extern crate serde_derive;

use async_trait::async_trait;
use serde::de::DeserializeOwned;
use serde::ser::Serialize;
use uuid::Uuid;

mod btree_seq;
mod certain_index_table;
mod index_table;
mod indexer;
mod item_id_table;
mod item_table;
mod iterator;
mod utils;

pub use anyhow::Result;
pub use indexer::Indexer;
pub use iterator::{Filter, IndexIterator, Iter};

pub type Bytes = Vec<u8>;
pub type ItemId = Uuid;

/// The value of an index column.
/// Let `TableItem` has the `login` index. This structure represents a value of the `login` property.
#[derive(Clone, Debug, Deserialize, Eq, Ord, PartialEq, PartialOrd, Serialize)]
#[serde(untagged)]
pub enum IndexValue {
    Number(u64),
    String(String),
}

impl From<String> for IndexValue {
    fn from(string: String) -> Self {
        IndexValue::String(string)
    }
}

impl From<u64> for IndexValue {
    fn from(number: u64) -> Self {
        IndexValue::Number(number)
    }
}

/// The index of an item.
#[derive(Deserialize, Serialize)]
pub struct ItemIndex {
    /// The index name.
    pub index_name: &'static str,
    /// The value of the [`ItemIndex::index_name`] index.
    pub index_value: IndexValue,
    /// Whether the [`ItemIndex::index_name`] index is unique.
    pub unique: bool,
}

/// A table item trait.
/// This trait provides the way to get values of the table indexes.
///
/// # Impl
///
/// The best way to implement the `TableItem` trait is to use a derive macro like:
/// ```ignore
/// #[derive(TableItem)]
/// struct User {
///     #[unique_index]
///     id: u64,
///     #[index]
///     login: String,
///     age: u8,
///     #[index]
///     nickname: String,
/// }
/// ```
/// Unfortunately, there is no such macro yet.
pub trait TableItem: DeserializeOwned + Serialize {
    /// Whether the given index is unique.
    fn is_unique(index_name: &'static str) -> Option<bool>;

    /// Get indexes of the item.
    fn indexes(&self) -> Vec<ItemIndex>;
}

/// A key-value store trait.
#[async_trait]
pub trait KeyValueStore {
    async fn get(&mut self, key: Bytes) -> Result<Option<Bytes>>;

    async fn set(&mut self, key: Bytes, value: Bytes) -> Result<()>;
}

pub trait Serializer: Default {
    fn serialize<T: Serialize>(&mut self, value: T) -> Result<Bytes>;

    fn deserialize<T: DeserializeOwned>(&mut self, bytes: Bytes) -> Result<T>;
}
