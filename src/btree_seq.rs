use serde::de::{SeqAccess, Visitor};
use serde::ser::{Serialize, SerializeSeq, Serializer};
use serde::{Deserialize, Deserializer};
use std::collections::BTreeMap;
use std::fmt::Formatter;
use std::marker::PhantomData;
use std::ops::{Deref, DerefMut};

/// The wrapper over `BTreeMap` that serializes and deserializes as a sequence.
/// This wrapper can be used if the `K` key is not a string,
/// but there is a need to serialize a sequence into JSON format.
#[derive(Debug)]
pub struct BTreeSequence<K, V>(BTreeMap<K, V>);

impl<K, V> From<BTreeSequence<K, V>> for BTreeMap<K, V> {
    fn from(seq: BTreeSequence<K, V>) -> Self {
        seq.0
    }
}

impl<K: Serialize, V: Serialize> Serialize for BTreeSequence<K, V> {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        let mut seq = serializer.serialize_seq(Some(self.0.len()))?;
        for (k, v) in self.0.iter() {
            seq.serialize_element(&(k, v))?;
        }
        seq.end()
    }
}

impl<'de, K, V> Deserialize<'de> for BTreeSequence<K, V>
where
    K: Deserialize<'de> + Ord,
    V: Deserialize<'de>,
{
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: Deserializer<'de>,
    {
        struct BTreeSequenceVisitor<K, V> {
            phantom: PhantomData<(K, V)>,
        }

        impl<'de, K, V> Visitor<'de> for BTreeSequenceVisitor<K, V>
        where
            K: Deserialize<'de> + Ord,
            V: Deserialize<'de>,
        {
            type Value = BTreeSequence<K, V>;

            fn expecting(&self, formatter: &mut Formatter) -> std::fmt::Result {
                write!(formatter, "a sequence of key-value pairs")
            }

            #[inline]
            fn visit_seq<A>(self, mut access: A) -> Result<Self::Value, A::Error>
            where
                A: SeqAccess<'de>,
            {
                let mut values: BTreeMap<K, V> = BTreeMap::new();

                while let Some((key, value)) = access.next_element()? {
                    values.insert(key, value);
                }

                Ok(BTreeSequence(values))
            }
        }

        let seq = deserializer.deserialize_seq(BTreeSequenceVisitor::<K, V> {
            phantom: PhantomData::default(),
        })?;
        Ok(seq)
    }
}

impl<K: Ord, V> Default for BTreeSequence<K, V> {
    fn default() -> Self {
        BTreeSequence::new()
    }
}

impl<K: Ord, V> BTreeSequence<K, V> {
    pub fn new() -> Self {
        BTreeSequence(BTreeMap::new())
    }
}

impl<K, V> Deref for BTreeSequence<K, V> {
    type Target = BTreeMap<K, V>;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

impl<K, V> DerefMut for BTreeSequence<K, V> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
