use crate::certain_index_table::{
    CertainIndexTableItem, CertainIndexTableKey, CertainUniqueIndexTableItem,
};
use crate::index_table::{IndexName, IndexTableItem, UniqueIndexTableItem};
use crate::item_id_table::{ItemIdTable, ItemIdTableKey};
use crate::item_table::ItemTable;
use crate::iterator::{Find, Iter};
use crate::utils::{load_table_item, load_table_item_or_default, upload_table_item, KeyValueItem};
use crate::{IndexValue, ItemId, ItemIndex, KeyValueStore, Serializer, TableItem};
use anyhow::{anyhow, bail, Result};
use futures::lock::{Mutex as AsyncMutex, MutexGuard as AsyncMutexGuard};
use std::marker::PhantomData;
use std::sync::Arc;

pub type IndexerLock<'a, Item, Store, Ser> = AsyncMutexGuard<'a, IndexerImpl<Item, Store, Ser>>;

pub struct Indexer<Item: TableItem, Store: KeyValueStore, Ser: Serializer> {
    inner: Arc<AsyncMutex<IndexerImpl<Item, Store, Ser>>>,
}

impl<Item: TableItem, Store: KeyValueStore, Ser: Serializer> Indexer<Item, Store, Ser> {
    pub fn from_store(store: Store) -> Self {
        let inner = IndexerImpl {
            store,
            phantom: PhantomData::default(),
        };
        Indexer {
            inner: Arc::new(AsyncMutex::new(inner)),
        }
    }

    pub fn __store(self) -> Result<Store, Self> {
        Arc::try_unwrap(self.inner)
            .map(|inner| inner.into_inner().store)
            .map_err(|inner| Self { inner })
    }

    pub async fn insert(&self, item: Item) -> Result<ItemId> {
        let mut indexer = self.inner.lock().await;
        indexer.insert_item(item).await
    }

    pub async fn iter(&self) -> Iter<'_, Item, Store, Ser> {
        let indexer = Some(self.inner.lock().await);
        Iter { indexer }
    }

    pub async fn find(
        &self,
        index_name: &'static str,
        index_value: IndexValue,
    ) -> Result<Find<'_, Item, Store, Ser>> {
        let index = ItemIndex {
            index_name,
            index_value,
            unique: Item::is_unique(index_name).ok_or_else(|| anyhow!("Unknown index name"))?,
        };

        let indexer = Some(self.inner.lock().await);
        Ok(Find { indexer, index })
    }
}

pub struct IndexerImpl<Item, Store, Ser> {
    store: Store,
    phantom: PhantomData<(Item, Ser)>,
}

impl<Item, Store, Ser> IndexerImpl<Item, Store, Ser> {
    pub(crate) fn store(&mut self) -> &mut Store {
        &mut self.store
    }
}

impl<Item, Store, Ser> IndexerImpl<Item, Store, Ser>
where
    Item: TableItem,
    Store: KeyValueStore,
    Ser: Serializer,
{
    pub async fn insert_item(&mut self, item: Item) -> Result<ItemId> {
        let item_table_item = ItemTable::new(item);
        let item_id = item_table_item.item_id();

        // TODO revert changes if an error occurred.
        // For simplicity, let's just exit from the function before the item is not inserted.
        // In my opinion, it's better to have indexes pointing to non-exist items
        // rather than have items that are not indexed.
        for index in item_table_item.item().indexes() {
            if index.unique {
                self.insert_unique_index(index, item_id).await?;
            } else {
                self.insert_index(index, item_id).await?;
            }
        }

        self.insert_item_id(item_id).await?;
        upload_table_item(&mut self.store, &mut Ser::default(), item_table_item).await?;
        Ok(item_id)
    }

    async fn insert_item_id(&mut self, item_id: ItemId) -> Result<()> {
        let mut serializer = Ser::default();
        let mut item_id_table_item: ItemIdTable =
            load_table_item_or_default(&mut self.store, &mut serializer, ItemIdTableKey).await?;
        if !item_id_table_item.insert(item_id) {
            bail!("An item with the same ItemId '{}' exists already", item_id);
        }
        upload_table_item(&mut self.store, &mut serializer, item_id_table_item).await?;
        Ok(())
    }

    async fn insert_index(&mut self, index: ItemIndex, item_id: ItemId) -> Result<()> {
        assert!(!index.unique);
        let mut serializer = Ser::default();

        // Insert the index into the table of indexes.
        let mut index_table_item: IndexTableItem = load_table_item_or_default(
            &mut self.store,
            &mut serializer,
            IndexName::new(index.index_name),
        )
        .await?;
        index_table_item.insert_index(index.index_value.clone(), item_id);
        upload_table_item(&mut self.store, &mut serializer, index_table_item).await?;

        // Insert the index value into the certain index table.
        let mut certain_index_table_item: CertainIndexTableItem = load_table_item_or_default(
            &mut self.store,
            &mut serializer,
            CertainIndexTableKey {
                index_name: index.index_name.to_owned(),
                index_value: index.index_value.clone(),
            },
        )
        .await?;
        certain_index_table_item.insert_item_id(item_id);
        upload_table_item(&mut self.store, &mut serializer, certain_index_table_item).await
    }

    async fn insert_unique_index(&mut self, index: ItemIndex, item_id: ItemId) -> Result<()> {
        assert!(index.unique);
        let mut serializer = Ser::default();

        // Insert the index into the table of unique indexes.
        let mut unique_index_table_item: UniqueIndexTableItem = load_table_item_or_default(
            &mut self.store,
            &mut serializer,
            IndexName::new(index.index_name),
        )
        .await?;
        if let Some(item_id) =
            unique_index_table_item.insert_index(index.index_value.clone(), item_id)
        {
            bail!(
                "An item with the same index '{}:{:?}' exists already: '{}'",
                index.index_name,
                index.index_value,
                item_id
            );
        }
        upload_table_item(&mut self.store, &mut serializer, unique_index_table_item).await?;

        // Check if there is no items with the same unique certain index.
        let certain_index_table_key = CertainIndexTableKey {
            index_name: index.index_name.to_owned(),
            index_value: index.index_value.clone(),
        };
        if let Some(CertainUniqueIndexTableItem { .. }) = load_table_item(
            &mut self.store,
            &mut serializer,
            certain_index_table_key.clone(),
        )
        .await?
        {
            bail!(
                "An item with the same index '{}:{:?}' exists already: '{}'",
                index.index_name,
                index.index_value,
                item_id
            );
        }

        // Point the unique index value to the given `item_id`.
        let certain_unique_index_table_item =
            CertainUniqueIndexTableItem::from_key_value(certain_index_table_key, item_id);
        upload_table_item(
            &mut self.store,
            &mut serializer,
            certain_unique_index_table_item,
        )
        .await
    }
}
