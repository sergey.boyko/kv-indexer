use crate::utils::{KeyValueItem, StoreKey, WithDefaultValueIfNotFound};
use crate::{IndexValue, ItemId};

/// The key of `CertainIndexTableItem`.
/// Let `TableItem` has the `email` index.
/// Then this structure represents a pair `("email", "example@gmail.com")`.
#[derive(Clone, Deserialize, Debug, Serialize)]
pub(crate) struct CertainIndexTableKey {
    pub(crate) index_name: String,
    pub(crate) index_value: IndexValue,
}

/// The item of a table that consists of the `CertainIndexTableKey` key and the `ItemId` value.
/// Let `TableItem` has the `email` index.
/// Then this structure will represent a row:
/// | "email", "example@gmail.com" | { "id": item1_id } |
#[derive(Serialize)]
pub(crate) struct CertainUniqueIndexTableItem {
    key: CertainIndexTableKey,
    value: ItemId,
}

impl KeyValueItem for CertainUniqueIndexTableItem {
    type Key = CertainIndexTableKey;
    type Value = ItemId;

    fn key_into_store_key(key: Self::Key) -> StoreKey {
        StoreKey::CertainUniqueIndex(key)
    }

    fn into_key_value(self) -> (Self::Key, Self::Value) {
        (self.key, self.value)
    }

    fn from_key_value(key: Self::Key, value: Self::Value) -> Self {
        CertainUniqueIndexTableItem { key, value }
    }
}

/// The item of a table that consists of the `CertainIndexTableKey` key and the `Vec<ItemId>` value.
/// Let `TableItem` has the `email` index.
/// Then this structure will represent a row:
/// | "email", "example@gmail.com" | { "ids": item1_id, item2_id, ... } |
#[derive(Serialize)]
pub struct CertainIndexTableItem {
    key: CertainIndexTableKey,
    value: Vec<ItemId>,
}

impl KeyValueItem for CertainIndexTableItem {
    type Key = CertainIndexTableKey;
    type Value = Vec<ItemId>;

    fn key_into_store_key(key: Self::Key) -> StoreKey {
        StoreKey::CertainIndex(key)
    }

    fn into_key_value(self) -> (Self::Key, Self::Value) {
        (self.key, self.value)
    }

    fn from_key_value(key: Self::Key, value: Self::Value) -> Self {
        CertainIndexTableItem { key, value }
    }
}

impl WithDefaultValueIfNotFound for CertainIndexTableItem {
    type Value = Vec<ItemId>;
}

impl CertainIndexTableItem {
    pub(crate) fn insert_item_id(&mut self, item_id: ItemId) {
        self.value.push(item_id)
    }
}
