use crate::utils::{KeyValueItem, StoreKey};
use crate::ItemId;
use serde::Serialize;

/// The item of a table that consists of the `ItemId` key and the `T` value.
pub(crate) struct ItemTable<T> {
    key: ItemId,
    pub(crate) value: T,
}

impl<T> KeyValueItem for ItemTable<T> {
    type Key = ItemId;
    type Value = T;

    fn key_into_store_key(key: Self::Key) -> StoreKey {
        StoreKey::Item(key)
    }

    fn into_key_value(self) -> (Self::Key, Self::Value) {
        (self.key, self.value)
    }

    fn from_key_value(key: Self::Key, value: Self::Value) -> Self {
        ItemTable { key, value }
    }
}

impl<T: Serialize> ItemTable<T> {
    pub(crate) fn new(value: T) -> Self {
        // Consider getting an item id as a SHA1 hash of the serialized `item`.
        // It can be useful if it's needed to store only unique items.
        let key = ItemId::new_v4();
        ItemTable { key, value }
    }

    pub(crate) fn item_id(&self) -> ItemId {
        self.key
    }

    pub(crate) fn item(&self) -> &T {
        &self.value
    }
}
